const graduados = [
    {
      nombre: "Juan",
      carrera: "Derecho",
      imagen: "/js/1.JPG"
    },
    {
      nombre: "Maria",
      carrera: "Medicina",
      imagen: "maria.jpg"
    },
    // más graduados...
  ];
  
  const contenedor = document.querySelector("#graduados");
  
  graduados.forEach(graduado => {
    const div = document.createElement("div");
    div.classList.add("graduado");
  
    const nombre = document.createElement("h2");
    nombre.textContent = graduado.nombre;
  
    const carrera = document.createElement("p");
    carrera.textContent = graduado.carrera;
  
    const img = document.createElement("img");
    img.src = graduado.imagen;
  
    div.appendChild(nombre);
    div.appendChild(carrera);
    div.appendChild(img);
  
    contenedor.appendChild(div);
  });